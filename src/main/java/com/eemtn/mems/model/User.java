package com.eemtn.mems.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by kasgul on 27.03.17.
 */
@Entity
@Table(name="memsuser")
public class User {

    @Id
    @GeneratedValue
    private int id;

    @Column
    private boolean administrator;

    @Column
    private String creditCardNumber;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column(unique = true)
    @NotNull
    private String phoneNumber;

    @Enumerated(value = EnumType.STRING)
    private UserStates userState;

    @Column
    @NotNull
    private  String pin;

    @Enumerated(value = EnumType.STRING)
    private CreditCardTypes creditCardType;

    public UserStates getUserState() {
        return userState;
    }

    public void setUserState(UserStates userState) {
        this.userState = userState;
    }

    public CreditCardTypes getCreditCardType() {
        return creditCardType;
    }

    public void setCreditCardType(CreditCardTypes creditCardType) {
        this.creditCardType = creditCardType;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isAdministrator() {
        return administrator;
    }

    public void setAdministrator(boolean administrator) {
        this.administrator = administrator;
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

}
