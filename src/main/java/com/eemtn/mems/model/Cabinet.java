package com.eemtn.mems.model;

import javax.persistence.*;

/**
 * Created by kasgul on 27.03.17.
 */
@Entity
public class Cabinet {

    @Id
    @GeneratedValue
    private int id;

    @Column
    private String address;

    @Column
    private float price;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }


}
