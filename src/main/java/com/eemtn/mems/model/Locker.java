package com.eemtn.mems.model;

import javax.persistence.*;

/**
 * Created by kasgul on 27.03.17.
 */
@Entity
public class Locker {

    @Id
    @GeneratedValue
    private int id;

    @Column(name="col")
    private int column;

    @Column
    private int row;

    @ManyToOne
    private Cabinet cabinet;

    @ManyToOne
    private User user;

    @Enumerated(value = EnumType.STRING)
    private LockerStates lockerState;

    public LockerStates getLockerState() {
        return lockerState;
    }

    public void setLockerState(LockerStates lockerState) {
        this.lockerState = lockerState;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Cabinet getCabinet() {
        return cabinet;
    }

    public void setCabinet(Cabinet cabinet) {
        this.cabinet = cabinet;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }
}
