package com.eemtn.mems.repositories;

import com.eemtn.mems.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 * Created by kasgul on 27.03.17.
 */
public interface UserRepository extends CrudRepository<User, Integer> {
    public User findByPhoneNumber(@Param("phoneNumber") String phoneNumber);
}
