package com.eemtn.mems.repositories;

import com.eemtn.mems.model.Locker;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by kasgul on 27.03.17.
 */
public interface LockerRepository extends CrudRepository<Locker, Integer> {
}
