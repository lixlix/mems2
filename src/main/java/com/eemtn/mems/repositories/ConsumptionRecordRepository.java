package com.eemtn.mems.repositories;

import com.eemtn.mems.model.ConsumptionRecord;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by kasgul on 27.03.17.
 */
public interface ConsumptionRecordRepository extends CrudRepository<ConsumptionRecord, Integer> {
}
