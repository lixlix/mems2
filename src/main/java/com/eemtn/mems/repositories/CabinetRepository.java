package com.eemtn.mems.repositories;

import com.eemtn.mems.model.Cabinet;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by kasgul on 27.03.17.
 */
public interface CabinetRepository extends CrudRepository<Cabinet, Integer> {
}
