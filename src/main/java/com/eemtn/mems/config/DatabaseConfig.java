package com.eemtn.mems.config;

import com.eemtn.mems.model.*;
import com.eemtn.mems.repositories.CabinetRepository;
import com.eemtn.mems.repositories.ConsumptionRecordRepository;
import com.eemtn.mems.repositories.LockerRepository;
import com.eemtn.mems.repositories.UserRepository;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.util.Date;

/**
 * Created by kasgul on 28.03.17.
 */
@Configuration
public class DatabaseConfig implements InitializingBean{

    @Autowired
    private LockerRepository lockerRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CabinetRepository cabinetRepository;

    @Autowired
    private ConsumptionRecordRepository consumptionRecordRepository;

    @Override
    public void afterPropertiesSet()
    {
        delete();
        insert();
    }

    private void delete()
    {
        for (ConsumptionRecord consumptionRecord : consumptionRecordRepository.findAll())
        {
            consumptionRecordRepository.delete(consumptionRecord);
        }

        for (Locker locker : lockerRepository.findAll())
        {
            lockerRepository.delete(locker);
        }

        for (Locker locker : lockerRepository.findAll())
        {
            lockerRepository.delete(locker);
        }

        for (Cabinet cabinet : cabinetRepository.findAll())
        {
            cabinetRepository.delete(cabinet);
        }

        for (User user : userRepository.findAll())
        {
            userRepository.delete(user);
        }
    }

    private void insert(){
        User admin = new User();
        admin.setFirstName("Mirbek");
        admin.setLastName("Borubaev");
        admin.setAdministrator(true);
        admin.setCreditCardNumber("2017 03 01");
        admin.setCreditCardType(CreditCardTypes.MASTER);
        admin.setPin("22");
        admin.setPhoneNumber("0554121212");
        userRepository.save(admin);

        User user = new User();
        user.setFirstName("Gulanda");
        user.setLastName("Kasymbekova");
        user.setAdministrator(false);
        user.setCreditCardType(CreditCardTypes.VISA);
        user.setPin("141");
        user.setCreditCardNumber("2017 02 01");
        user.setUserState(UserStates.REQUESTED);
        user.setPhoneNumber("0556121212");
        userRepository.save(user);

        Cabinet cabinet = new Cabinet();
        cabinet.setAddress("Germany, Leipzig, Brauerei Str.23/212");
        cabinet.setPrice(2);
        cabinetRepository.save(cabinet);

        Locker locker1 = new Locker();
        locker1.setColumn(1);
        locker1.setRow(1);
        locker1.setLockerState(LockerStates.EMPTY);
        locker1.setCabinet(cabinet);
        lockerRepository.save(locker1);

        Locker locker2 = new Locker();
        locker2.setColumn(2);
        locker2.setRow(1);
        locker2.setLockerState(LockerStates.EMPTY);
        locker2.setCabinet(cabinet);
        lockerRepository.save(locker2);

        ConsumptionRecord record1 = new ConsumptionRecord();
        record1.setStartTime(new Date());
        record1.setEndTime(new Date());
        record1.setLocker(locker1);
        record1.setUser(user);
        record1.setConsumption(10);
        consumptionRecordRepository.save(record1);

        ConsumptionRecord record2 = new ConsumptionRecord();
        record2.setStartTime(new Date());
        record2.setEndTime(new Date());
        record2.setLocker(locker2);
        record2.setUser(user);
        record2.setConsumption(5);
        consumptionRecordRepository.save(record2);
    }
}
