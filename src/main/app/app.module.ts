import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';

import { AppComponent } from './components/app/app.component';
import { CreditCardTypeInputComponent } from './components/credit-card-type-input/credit-card-type-input.component';
import { RegisterComponent } from './components/register/register.component';
import { CreditCardNumberValidator } from './directives/credit-card-number-validator/credit-card-number-validator.directive';
import { EqualValidator } from './directives/equal-validator/equal-validator.directive';
import { UnusedPhoneNumberValidator } from './directives/unused-phone-number-validator/unused-phone-number-validator.directive';

@NgModule({
  declarations: [
    AppComponent,
    CreditCardTypeInputComponent,
    RegisterComponent,
    CreditCardNumberValidator,
    EqualValidator,
    UnusedPhoneNumberValidator
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {

}
