import { Component, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => CreditCardTypeInputComponent),
        multi: true
    }],
    selector: 'credit-card-type-input',
    templateUrl: './credit-card-type-input.component.html'
})
export class CreditCardTypeInputComponent implements ControlValueAccessor {

    private propagateChange = (_: any) => { };

    private value = 'VISA'

    types = [{
        name: 'VISA',
        value: 'VISA',
        logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Visa_2014_logo_detail.svg/200px-Visa_2014_logo_detail.svg.png'
    }, {
        name: 'Mastercard',
        value: 'MASTER',
        logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/Mastercard-logo.svg/161px-Mastercard-logo.svg.png'
    }]

    public writeValue(obj: any) {
        if (obj) {
            this.value = obj;
        }
    }

    public registerOnChange(fn: any) {
        this.propagateChange = fn;
    }

    public registerOnTouched() { }

    select(value) {
        this.value = value
        this.propagateChange(value);
    }
}
