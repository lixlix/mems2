import { Component } from '@angular/core';
import { Http } from '@angular/http';

@Component({
  selector: 'register',
  templateUrl: './register.component.html'
})
export class RegisterComponent {

    model = {
        creditCardNumber: "",
        creditCardType: "VISA",
        firstName: "",
        lastName: "",
        phoneNumber: "",
        pin: undefined,
        pinConfirmation: undefined
    }

    constructor(private http: Http) {}

    submit() {

        this.http.post("/api/users", this.model).subscribe(() => {
            console.log("ok");
         }, (error) => {
            console.log("Saving failed ", error);
         });
    }
}
