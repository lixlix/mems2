import { Directive, forwardRef, Attribute, Input, OnChanges } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS, FormControl } from '@angular/forms';
@Directive({
    selector: '[validate-equal][ngModel]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: forwardRef(() => EqualValidator), multi: true }
    ]
})
export class EqualValidator implements Validator, OnChanges {
    @Input("target") target

    private myValue : String

    private otherValue : String

    private control : AbstractControl

    constructor( @Attribute('validateEqual') public validateEqual: string) {}

    ngOnChanges(changes: any) {

        if (changes.target)
        {
            this.otherValue = changes.target.currentValue;
        }

        if (this.control != null)
        {
            this.control.updateValueAndValidity();
        }
    }

    validate(control: AbstractControl): { [key: string]: any } {

        this.control = control;

        this.myValue = control.value;

        if (this.myValue != null && this.otherValue != null && this.myValue != this.otherValue)
        {
            return { "validateEqual": false }
        }

        return null;
    }
}
