import { Directive, forwardRef, Attribute } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';
@Directive({
    selector: '[valid-credit-card-number]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: forwardRef(() => CreditCardNumberValidator), multi: true }
    ]
})
export class CreditCardNumberValidator implements Validator {

    private pattern : RegExp = /\d{4}[- ]?\d{4}[- ]?\d{4}[- ]?\d{4}/g

    validate(c: AbstractControl): { [key: string]: any } {

        if (c.value == null ||  !c.value.match(this.pattern)) {
            return {"patternMismatch": true};
        }

        var sum : number = 0.0;
        var double = true;
        for (var i : number = 0; i < c.value.length; i++) {

            if (c.value[i] == '-' || c.value[i] == ' ')
            {
                continue;
            }

            var digit : number = parseInt(c.value[i]);

            // every 2nd number multiply with 2
            if (double) {
               digit *= 2;
            }
            double = !double;

            sum = sum + (digit > 9 ? digit - 9 : digit);
        }

        if (sum % 10 != 0) {
            return {"invalidChecksum": true};
        }

        return null;
    }
}
