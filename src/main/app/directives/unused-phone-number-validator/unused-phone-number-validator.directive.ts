import { Directive, forwardRef, Attribute, Input, OnChanges } from '@angular/core';
import { Validator, AbstractControl, NG_ASYNC_VALIDATORS, FormControl } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

@Directive({
    selector: '[unused-phone-number][ngModel]',
    providers: [
        { provide: NG_ASYNC_VALIDATORS, useExisting: forwardRef(() => UnusedPhoneNumberValidator), multi: true }
    ]
})
export class UnusedPhoneNumberValidator implements Validator {

    constructor(private http: Http) {}

    validate( c : AbstractControl ) : Observable<{[key : string] : any}> {

        if (!c.value) {
            return Observable.of(null);
        }

        return this.http.get("/api/users/search/findByPhoneNumber?phoneNumber="+c.value).map(
            (res:Response) => {
                  return { 'usedPhoneNumber' : true };
            }).catch((error: Response | any) => {
                if (error.status && error.status == 404) {
                    return Observable.of(null);
                } else {
                    return Observable.of({ 'usedPhoneNumber' : true });
                }
            });
    }
}
